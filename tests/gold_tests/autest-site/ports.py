
import socket
import subprocess

try:
    import queue as Queue
except ImportError:
    import Queue

g_ports = None  # ports we can use


def PortOpen(port, address=None):

    ret = False
    if address is None:
        address = "localhost"

    address = (address, port)

    try:
        s = socket.create_connection(address, timeout=.5)
        s.close()
        ret = True
    except socket.error:
        s = None
        ret = False
    except socket.timeout:
        s = None

    return ret


def setup_port_queue(amount=1000):
    global g_ports
    if g_ports is None:
        g_ports = Queue.LifoQueue()
    else:
        return
    try:
        dmin, dmax = subprocess.check_output(
            ["sysctl", "net.ipv4.ip_local_port_range"]).decode().split("=")[1].split()
        dmin = int(dmin)
        dmax = int(dmax)
    except:
        return

    rmin = dmin - 2000
    rmax = 65536 - dmax

    if rmax > amount:
        # fill in ports
        port = dmax + 1
        while port < 65536 and g_ports.qsize() < amount:
            # if port good:
            if not PortOpen(port):
                g_ports.put(port)
            port += 1
    if rmin > amount and g_ports.qsize() < amount:
        port = 2001
        while port < dmin and g_ports.qsize() < amount:
            # if port good:
            if not PortOpen(port):
                g_ports.put(port)
            port += 1

def get_port(obj, name):
    '''
    Get a port and set it to a variable on the object

    '''

    setup_port_queue()
    if g_ports.qsize():
        # get port
        port = g_ports.get()
        # assign to variable
        obj.Variables[name] = port
        # setup clean up step to recycle the port
        obj.Setup.Lambda(func_cleanup=lambda: g_ports.put(
            port), description="recycling port")
        return port

    # use old code
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    sock.bind(('', 0))  # bind to all interfaces on an ephemeral port
    port = sock.getsockname()[1]
    obj.Variables[name] = port
    return port
